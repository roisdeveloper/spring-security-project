CREATE SEQUENCE tbl_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.tbl_user_id_seq OWNER TO postgres;

CREATE TABLE tbl_user (
	id INTEGER NOT NULL PRIMARY KEY,
	username VARCHAR(255) NOT NULL UNIQUE,
	status BOOLEAN NOT NULL DEFAULT TRUE,
	created_at TIMESTAMP DEFAULT NULL,
	updated_at TIMESTAMP DEFAULT NULL
);


CREATE SEQUENCE tbl_password_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.tbl_password_id_seq OWNER TO postgres;

CREATE TABLE tbl_password (
	id INTEGER NOT NULL PRIMARY KEY,
	id_user INTEGER NOT NULL,
	password VARCHAR(255) NOT NULL,
	created_at TIMESTAMP DEFAULT NULL,
	updated_at TIMESTAMP DEFAULT NULL,
	FOREIGN KEY (id_user) REFERENCES tbl_user(id)
);

CREATE SEQUENCE tbl_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.tbl_role_id_seq OWNER TO postgres;

CREATE TABLE tbl_role (
	id INTEGER NOT NULL PRIMARY KEY,
	role_name VARCHAR(255) NOT NULL UNIQUE,
	role_desc VARCHAR(255) NOT NULL UNIQUE,
	status BOOLEAN NOT NULL DEFAULT TRUE,
	created_at TIMESTAMP DEFAULT NULL,
	updated_at TIMESTAMP DEFAULT NULL
);

CREATE SEQUENCE tbl_user_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.tbl_user_role_id_seq OWNER TO postgres;

CREATE TABLE tbl_user_role (
	id INTEGER NOT NULL PRIMARY KEY,
	id_user INTEGER NOT NULL UNIQUE,
	id_role INTEGER NOT NULL UNIQUE,
	FOREIGN KEY(id_user) REFERENCES tbl_user(id),
	FOREIGN KEY(id_role) REFERENCES tbl_role(id)
);

CREATE SEQUENCE tbl_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.tbl_permission_id_seq OWNER TO postgres;

CREATE TABLE tbl_permission (
	id INTEGER NOT NULL PRIMARY KEY,
	permission_name VARCHAR(255) NOT NULL UNIQUE,
	permission_desc VARCHAR(255) NOT NULL,
	status BOOLEAN NOT NULL DEFAULT TRUE,
	created_at TIMESTAMP DEFAULT NULL,
	updated_at TIMESTAMP DEFAULT NULL
);

CREATE SEQUENCE tbl_role_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.tbl_role_permission_id_seq OWNER TO postgres;

CREATE TABLE tbl_role_permission (
	id INTEGER NOT NULL PRIMARY KEY,
	id_role INTEGER NOT NULL UNIQUE,
	id_permission INTEGER NOT NULL UNIQUE,
	FOREIGN KEY(id_role) REFERENCES tbl_role(id),
	FOREIGN KEY(id_permission) REFERENCES tbl_permission(id)
);