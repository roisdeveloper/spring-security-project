package com.springsecurity.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class GlobalController {
	private static final Logger LOG=LogManager.getLogger(GlobalController.class);
	
	@GetMapping
	public void getIndexPage() {
		LOG.info("LOG : All granted");
	}
	
	@GetMapping(value = "/admin")
	public void getAdminPage() {
		LOG.info("LOG : admin only");
	}
	
	@GetMapping(value = "/user")
	public void getUserPage() {
		LOG.info("LOG : admin only");
	}
	
	@GetMapping(value = "/manager")
	public void getManagerPage() {
		LOG.info("LOG : manager only");
	}
}
