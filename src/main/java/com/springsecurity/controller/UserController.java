package com.springsecurity.controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springsecurity.entity.User;
import com.springsecurity.service.UserService;

@RestController
@RequestMapping(value = "/api/user")
public class UserController {
	
	private static final Logger LOG=LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService service;
	
	@GetMapping(value = "/list",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<User>> showAllUser(Pageable pageable){
		Page<User> users=service.getAllUser(pageable);
		if(users.isEmpty()) {
			LOG.info("User not availabe");
			return new ResponseEntity<Page<User>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Page<User>>(users,new HttpHeaders(),HttpStatus.OK);
	}
	
	
}
