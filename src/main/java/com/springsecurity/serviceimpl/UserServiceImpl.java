package com.springsecurity.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.springsecurity.entity.User;
import com.springsecurity.repository.UserRepository;
import com.springsecurity.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository repository;
	
	@Override
	public Page<User> getAllUser(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public User getUserById(Long id) {
		return repository.findById(id).get();
	}

}
