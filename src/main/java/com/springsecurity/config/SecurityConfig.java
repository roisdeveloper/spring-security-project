package com.springsecurity.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private PasswordEncoder encoder;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		//http.authorizeRequests().anyRequest().permitAll();
				/*
				  .antMatchers("/api/admin/**").access("hasRole('ADMIN')")
				  .antMatchers("/api/user/**").access("hasRole('USER')")
				  .antMatchers("/api/manager/**").access("hasRole('MANAGER')")
				 

				
				.antMatchers("/api/**").hasRole("ADMIN")
				.antMatchers("/api/user/**").hasRole("USER")
				.antMatchers("/api/manager/**").hasRole("MANAGER")

				.anyRequest().authenticated().and().formLogin();*/
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser("user").password(encoder.encode("password")).roles("USER");
		auth.inMemoryAuthentication().withUser("manager").password(encoder.encode("password")).roles("MANAGER");
		auth.inMemoryAuthentication().withUser("admin").password(encoder.encode("password")).roles("ADMIN");
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}