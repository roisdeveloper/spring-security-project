package com.springsecurity.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UserRoleId implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(name = "id_user")
	private Long idUser;
	@Column(name = "id_role")
	private Long idRole;
	
	public UserRoleId() {}
	

	public UserRoleId(Long idUser, Long idRole) {
		this.idUser = idUser;
		this.idRole = idRole;
	}


	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public Long getIdRole() {
		return idRole;
	}

	public void setIdRole(Long idRole) {
		this.idRole = idRole;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public boolean equals(Object obj) {
		if(this==obj) {
			return true;
		}
		
		if(obj == null || getClass() != obj.getClass()) {
			return false;
		}
		
		UserRoleId that=(UserRoleId) obj;
		return Objects.equals(idUser,that.idUser) && Objects.equals(idRole, that.idRole);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(idUser,idRole);
	}
	
}
