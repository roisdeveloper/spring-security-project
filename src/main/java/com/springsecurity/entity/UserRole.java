package com.springsecurity.entity;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "tbl_user_role",schema = "public")
public class UserRole {
	@EmbeddedId
	private UserRoleId id;

	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("idUser")
	private User user;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("idRole")
	private Role role;
	
	@CreationTimestamp
	@Column(name = "created_on")
	private Date createdOn;
	
	public UserRole() {}
	
	public UserRole(User user,Role role) {
		this.user=user;
		this.role=role;
		this.id=new UserRoleId(user.getId(),role.getId());
	}

	public UserRoleId getId() {
		return id;
	}

	public void setId(UserRoleId id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this==obj) {
			return true;
		}
		
		if(obj == null || getClass() != obj.getClass()) {
			return false;
		}
		
		UserRole that=(UserRole) obj;
		return Objects.equals(user,that.user) && Objects.equals(role, that.role);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(user,role);
	}
}
