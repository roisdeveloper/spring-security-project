package com.springsecurity.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.springsecurity.entity.User;

public interface UserService {
	Page<User> getAllUser(Pageable pageable);
	User getUserById(Long id);
}
